# What Is SML?
It stands for Simple Modeling Language. It is a substitute for UML.
# Rationale
UTL is too complex.
One often needs to combine aspects of multiple UML view types in a single diagram, to illustrate how behavior uses types and produces object instances.
Creating complete diagrams is not the goal: providing clear explanation of scenarios is the goal.

The goal is not to be able to generate code from an SML diagram; the goal is the help another human to understand the structure and behavior of a piece of software.
# History
I have been using the SML syntax in my books since 1997, e.g., [this one](https://www.amazon.com/High-Assurance-Design-Architecting-Enterprise-Applications/dp/0321793277) and [this one](https://www.amazon.com/Advanced-Java-Development-Enterprise-Applications/dp/0130848751/ref=dp_ob_title_bk). I have decided to define it and share it.

A nice example of its use is [here](https://github.com/ScaledMarkets/dabl/tree/master/java/scaledmarkets/dabl).
# Square
A thing, such as a class instance (object), or a static class or singleton.
# Angular Shape
A contract or template of some kind, e.g., a class, interface or protocol.
# Arrow
Some kind of relationship; the annotation says what kind. The endpoints indicate ownership (or not). Annotations may indicate cardinality.

# Example

![](SML1.png )

1. Illustrates an object.
2. An interface. 
3. A relationship. The annotation “Is a” indicates that the class implements the interface.
4. Indicates that a class instance references another class instance; the relationship is non-owning because the referencing class has an open diamond.
5. Indicates that a class instance owns a collection of other instances, with a one-to-many relationship.

# Expanding a Thing

![](SML2.png )

Expands an instance to reveal that it contains two state variables. Each variable is shown to reference (via an owning relationship) another object.

# Rounded Rectangle
A rounded rectangle indicates behavior - usually a method or a function.

![](SML3.png )

A summary of the behavior can be written next to it as an annotation. Parts of that annotation can be exploded, with an additional annotation, perhaps shaded to separate it.


